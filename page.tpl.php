<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
	<head>
		<title><?php print $head_title ?></title>
		<?php print $head ?>
		<?php print $styles ?>
<!--[if IE]>
		<link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme() ?>/css/ie.css" />
<![endif]-->
		<?php print $scripts ?>
		<script type="text/javascript">
			$(document).ready(function() {
				//Activate FancyBox
				$("a#custom_3").fancybox({
					'zoomSpeedIn':	 0, 
					'zoomSpeedOut': 0
				});
			});
		</script>
		<!--[if lte IE 6]>
			<script type="text/javascript"> 
			   $(document).ready(function(){ 
        			$(document).pngFix(); 
    			}); 
			</script> 
		<![endif]-->
		<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
		<?php
			$msg = "";

			$site_name = variable_get('site_name', 'Launchpad');

			$fb_user = theme_get_setting('launchpad_feedburner_user');
			if (!$fb_user) {
      				$msg .= t(" User not set.");
			}

			$fb_fid = theme_get_setting('launchpad_feedburner_fid');
			if (!is_numeric($fb_fid) && $fb_fib > 0) {
      				$msg .= t(" ID not set.");
			}
			
			if (empty($msg)) { ?>
		<link rel="alternate" type="application/rss+xml" 
		      	href="http://feeds.feedburner.com/<?php print $fb_user; ?>" 
   			title="<?php print $site_name; ?> RSS feed" />
		<?php 	} ?>
	</head>
	<body>
		<div id="wrapper">
		<?php if (!empty($msg)) { ?>
			<div id="topinfo" class="error"><p><?php 
			print t("Launchpad Theme is not configured:").$msg; ?></p></div>
		<?php } ?>
			<div id="main">
				<h1><span><?php print t('You&rsquo;ve found'); ?></span> 
                                   <?php print $site_name; ?></h1>
				<p><?php print t('We&rsquo;re not <em>quite</em> there yet &mdash; <em>but we&rsquo;re getting there!</em> &mdash; and we <strong>really</strong> want you  to know when we&rsquo;re ready. Here&rsquo;s how to stay updated:'); ?></p>
			</div><!-- #main -->
			<div id="options-wrap">
				<div id="subscribe-options">
					<p class="rss-subscribe"><a id="custom_3" href="http://feeds.feedburner.com/<?php print $fb_user; ?>" 
						title="<?php print $site_name; ?> RSS feed"
                                                rel="alternate" type="application/rss+xml"><?php print t('Subscribe with '); ?>
						<img src="<?php print base_path() . path_to_theme() ?>/images/rss-icon.gif" 
						alt="RSS" /> <?php print t('in your feed reader'); ?></a></p>
					<form action="http://www.feedburner.com/fb/a/emailverify" method="post" target="popupwindow" 
							onsubmit="window.open('http://www.feedburner.com/fb/a/emailverifySubmit?feedId=', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
						<p class="form-label"><?php print t('Enter your email address'); ?>:</p><p><input type="text" style="width:140px" 
							name="email"/></p>
						<input type="hidden" value="http://feeds.feedburner.com/~e?ffid=<?php print $fb_fid; ?>" name="url"/>
						<input type="hidden" value="<?php print $site_name; ?>" name="title"/>
						<input type="hidden" name="loc" value="<?php print $language; ?>"/>
						<input id="submit" type="submit" value="<?php print t('Subscribe'); ?>" />
					</form>
				</div><!-- #subscribe-options -->
			</div><!-- #options-wrap -->
			<div id="page-info">
				<p><?php print t('Powered by'); ?> <a href="http://www.drupal.org/" title="Drupal">Drupal</a>, <a 
					href="http://www.feedburner.com/" title="FeedBurner">FeedBurner</a> &amp; <a 
					href="http://drupal.org/project/launchpad" title="Launchpad for Drupal">LaunchPad</a>. <?php print t('Content'); ?> &copy; <a 
					href="#"><?php print $site_name; ?></a>. <?php print t('All rights reserved'); ?>.</p>
			</div><!-- #page-info -->
		</div><!-- #wrapper -->
	</body>
</html>
<!--// 
	Launchpad for Drupal by iPublicis.com from 
	Launchpad for Wordpress by ThemeShaper.com
	$Id$
 //-->
