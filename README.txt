
-- SUMMARY --

Drupal Launchpad Theme is a version of the original Launchpad theme made by
ThemeShaper.com for Wordpress. It aims to suply a placeholder theme for 
websites that are under development or under maintenance.
A great thank you goes to Ian Stewart for the original idea.

For a full description visit the project page:
  http://drupal.org/project/launchpad
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/launchpad


-- REQUIREMENTS --

You MUST have a FeedBurner account. Get one at:
  https://www.feedburner.com/fb/a/register

You MUST have the ThemeSettingsAPI module installed:
  http://drupal.org/project/themesettingsapi


-- INSTALLATION --

* Theme version install as usual, see http://drupal.org/node/456 for further information.
* Mod version, please read below.


-- CONFIGURATION --

* IMPORTANT! Before anything set another theme as your admin theme. Failing to do 
  so will block you out unless you remove Launchpad theme from server.

* Configure Launchpad and set it as the default theme. Don't forget to set your 
  Feedburner account too.


-- CUSTOMIZATION --

* If want to use a modded version with your own theme use the following steps:

  1) Copy page.tpl.php from the "launchpad" directory as page-offline.tpl.php 
     into your own theme's folder
  2) Copy settings.php from the "launchpad" directory into your theme's folder
  3) Add the functions from template.php (in the "launchpad" directory) to your
     own theme's template.php
  4) Install ThemeSettingsAPI module
  5) Add the FeedBurner account information in your theme admin config page
  6) Set you site as offline to see it working


-- TROUBLESHOOTING --

See http://drupal.org/project/issues/launchpad for help.
We do not support the MODed version besides its own code errors.


-- CONTACT --

Current maintainers:
* Lopo Lencastre de Almeida (humaneasy) - http://drupal.org/user/26117

This project has been sponsored by:
* iPublicis
  Consulting and planning of Drupal powered sites, we offer installation, development, 
  theming, customization, SEO planning and hosting to get you started. 
  Besides Drupal, advertising and FLOSS consulting.
  Visit http://www.ipublicis.com to contact us.


-- NOTICE --

Launchpad for Drupal by iPublicis.com based on
Launchpad for Wordpress by ThemeShaper.com
Theme is relased under the GNU/GPL version 2 or above.
