<?php
// Launchpad for Drupal by iPublicis.com
// Original for Wordpress by ThemeShaper.com

if (is_null(theme_get_setting('launchpad_feedburner_user'))) {
  global $theme_key;

  // Save default theme settings
  $defaults = array(
    'launchpad_feedburner_user' => '',
    'launchpad_feedburner_fid'  => 0,
  );

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge(theme_get_settings($theme_key), $defaults)
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);

}

// Theme Styles
drupal_add_css(drupal_get_path('theme', 'launchpad') . '/css/style.css', 'theme');
drupal_add_css(drupal_get_path('theme', 'launchpad') . '/css/fancy.css', 'theme');

// Theme Scripts
drupal_add_js(drupal_get_path('theme', 'launchpad') . '/js/jquery.fancybox.js', 'theme');
drupal_add_js(drupal_get_path('theme', 'launchpad') . '/js/jquery.pngFix.js', 'theme');
