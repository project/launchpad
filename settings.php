<?php
// Launchpad for Drupal by iPublicis.com
// Original for Wordpress by ThemeShaper.com

function phptemplate_settings($saved_settings) {

  $settings = theme_get_settings('launchpad');

  $defaults = array(
    'launchpad_feedburner_user' => '',
    'launchpad_feedburner_fid'  => 0,
  );

  $settings = array_merge($defaults, $settings);

  $form['launchpad_feedburner_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Your FeedBurner Username'),
    '#default_value' => $settings['launchpad_feedburner_user'],
    '#size' => 40,
    '#maxlength' => 75,
  );

  $form['launchpad_feedburner_fid'] = array(
    '#type' => 'textfield',
    '#title' => t('Your FeedBurner ID'),
    '#default_value' => $settings['launchpad_feedburner_fid'],
    '#size' => 15,
    '#maxlength' => 20,
  );

  return $form;
}


